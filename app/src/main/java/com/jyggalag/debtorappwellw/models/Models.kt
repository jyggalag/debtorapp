package com.jyggalag.debtorappwellw.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "my_debts")
data class MyDebt(
    @PrimaryKey
    var uid: Long = 0L,
    @ColumnInfo
    var owner: String,
    @ColumnInfo
    var partnerName: String,
    @ColumnInfo
    var partnerPhone: String,
    @ColumnInfo
    var partnerEmail: String,
    @ColumnInfo
    var debtType: String,
    @ColumnInfo
    var deadline: String,
    @ColumnInfo
    var startTime: String
)

@Entity(tableName = "foreign_debts")
data class ForeignDebt(
    @PrimaryKey
    var uid: Long = 0L,
    @ColumnInfo
    var owner: String,
    @ColumnInfo
    var partnerName: String,
    @ColumnInfo
    var partnerPhone: String,
    @ColumnInfo
    var partnerEmail: String,
    @ColumnInfo
    var debtType: String,
    @ColumnInfo
    var deadline: String,
    @ColumnInfo
    var startTime: String
)