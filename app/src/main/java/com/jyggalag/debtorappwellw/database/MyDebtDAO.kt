package com.jyggalag.debtorappwellw.database

import androidx.room.*
import com.jyggalag.debtorappwellw.models.MyDebt

@Dao
interface MyDebtDAO {

    @Insert
    fun addMyDebt(myDebt: MyDebt)

    @Update
    fun updateMyDebt(myDebt: MyDebt)

    @Query("DELETE FROM my_debts WHERE uid=:uid")
    fun deleteMyDebt(uid: Long)

    @Query("SELECT * FROM my_debts WHERE owner=:owner ORDER BY deadline ASC")
    fun retrieveMyDebts(owner: String): MutableList<MyDebt>

    @Query("UPDATE my_debts SET debtType=:value WHERE uid=:uid")
    fun updateMyDebtValue(uid: Long, value: String)

}