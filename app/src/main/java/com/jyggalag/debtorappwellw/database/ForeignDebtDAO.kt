package com.jyggalag.debtorappwellw.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.jyggalag.debtorappwellw.models.ForeignDebt

@Dao
interface ForeignDebtDAO {

    @Insert
    fun addForeignDebt(foreignDebt: ForeignDebt)

    @Update
    fun updateForeignDebt(foreignDebt: ForeignDebt)

    @Query("DELETE FROM foreign_debts WHERE uid=:uid")
    fun deleteForeignDebt(uid: Long)

    @Query("SELECT * FROM foreign_debts WHERE owner=:owner ORDER BY deadline ASC")
    fun retrieveForeignDebts(owner: String): MutableList<ForeignDebt>

    @Query("UPDATE foreign_debts SET debtType=:value WHERE uid=:uid")
    fun updateForeignDebtValue(uid: Long, value: String)

}