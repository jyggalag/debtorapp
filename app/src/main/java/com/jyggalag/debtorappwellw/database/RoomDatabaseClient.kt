package com.jyggalag.debtorappwellw.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.jyggalag.debtorappwellw.models.ForeignDebt
import com.jyggalag.debtorappwellw.models.MyDebt

const val DATABASE_SCHEMA_VERSION = 1
const val DB_NAME = "local_db"

@Database(version = DATABASE_SCHEMA_VERSION, entities = [MyDebt::class, ForeignDebt::class])
abstract class RoomDatabaseClient: RoomDatabase() {

    // place for DAO
    abstract fun myDebtDAO(): MyDebtDAO
    abstract fun foreignDebtDAO(): ForeignDebtDAO

    companion object{
        private var instance: RoomDatabaseClient? = null

        fun getInstance(context: Context): RoomDatabaseClient {
            if (instance == null) {
                instance = createDatabase(context)
            }
            return instance!!
        }

        // TODO: 28.07.2020 move away from main Thread Queries (Hint: Coroutines)
        private fun createDatabase(context: Context): RoomDatabaseClient {
            return Room.databaseBuilder(context, RoomDatabaseClient::class.java, DB_NAME)
                .build()
        }
    }
}