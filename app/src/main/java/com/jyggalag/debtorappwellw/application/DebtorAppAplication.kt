package com.jyggalag.debtorappwellw.application

import android.app.Application

class DebtorAppApplication: Application() {
    companion object {
        lateinit var instance: Application
    }
    init {
        instance = this
    }
}