package com.jyggalag.debtorappwellw.foundations

import com.jyggalag.debtorappwellw.ui.foreign_debts.ForeignDebtLocalModel
import com.jyggalag.debtorappwellw.ui.foreign_debts.IForeignDebtModel
import com.jyggalag.debtorappwellw.ui.my_debts.IMyDebtModel
import com.jyggalag.debtorappwellw.ui.my_debts.MyDebtLocalModel
import toothpick.Toothpick
import toothpick.config.Module

// this is a singleton
object ApplicationScope {
    // opening scope - this allows to inject model into it
    val scope = Toothpick.openScope(this).apply {
        // what classes will be supplying interface
        installModules(ApplicationModule)
    }
}

object ApplicationModule: Module() {
    init {
        bind(IMyDebtModel::class.java).toInstance(MyDebtLocalModel())
        bind(IForeignDebtModel::class.java).toInstance(ForeignDebtLocalModel())
    }
}