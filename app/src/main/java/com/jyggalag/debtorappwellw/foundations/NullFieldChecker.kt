package com.jyggalag.debtorappwellw.foundations

interface NullFieldChecker {
    fun hasNullField(): Boolean
}