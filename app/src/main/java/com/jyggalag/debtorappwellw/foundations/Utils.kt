package com.jyggalag.debtorappwellw.foundations

import android.os.Build
import android.widget.EditText
import androidx.annotation.RequiresApi
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

object DateUtils {

    val pattern = "dd.MM.yyyy"

    @RequiresApi(Build.VERSION_CODES.O)
    fun setToday(): String {
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ofPattern(pattern)
        return current.format(formatter)
    }

    fun updateDateInView(text: EditText, cal: Calendar) {
        val sdf = SimpleDateFormat(pattern, Locale.US)
        text.setText(sdf.format(cal.time))
    }

}

object MessageTo {

    fun produceRemindMessage(partnerName: String, deadLine: String, startTime: String, debtKind: String, debtType: String, owner: String): String =
        "Cześć $partnerName,\n\n" +
                "Przesyłam powiadomienie na temat pożyczki zawartej dnia $startTime" +
                (if (debtKind == NavigationActivity.FRAGMENT_MY_DEBT)  " przeze mnie"  else " przez Ciebie") +
                ".\n" +
                "\tprzedmiot: $debtType,\n" +
                "\ttermin: $deadLine\n" +
                "\nPozdrawiam, $owner"

    fun produceRemindSMS(partnerName: String, deadLine: String, startTime: String, debtKind: String, debtType: String, owner: String): String =
        "Cześć $partnerName. " +
                "Informuję o pożyczce zawartej dnia $startTime" +
                (if (debtKind == NavigationActivity.FRAGMENT_MY_DEBT)  " przeze mnie"  else " przez Ciebie") +
                ". " +
                "Przedmiot: $debtType. " +
                "Termin: $deadLine. " +
                "Pozdrawiam, $owner."

    fun producePaymentSMS(partnerName: String, deadLine: String, startTime: String, debtKind: String, debtType: String, owner: String): String =
        "Cześć $partnerName. " +
                "Potwierdzam, iż pożyczka zawarta dnia $startTime" +
                (if (debtKind == NavigationActivity.FRAGMENT_MY_DEBT)  " przeze mnie"  else " przez Ciebie") +
                " została spłacona. " +
                "Przedmiot: $debtType. " +
                "Termin: $deadLine. " +
                "Pozdrawiam, $owner."

    fun producePartialPaymentSMS(partnerName: String, deadLine: String, startTime: String, debtKind: String, debtType: String, owner: String): String =
        "Cześć $partnerName. " +
                "Informuję o częściowej spłacie pożyczki z dnia $startTime" +
                (if (debtKind == NavigationActivity.FRAGMENT_MY_DEBT)  " przeze mnie"  else " przez Ciebie") +
                ". " +
                "Pozostała kwota: $debtType. " +
                "Termin: $deadLine. " +
                "Pozdrawiam, $owner."
}