package com.jyggalag.debtorappwellw.firebase

import com.google.firebase.auth.FirebaseAuth

class FirebaseSource {
    private val nameTAG = this::class.java.toString()
    private lateinit var auth: FirebaseAuth
}