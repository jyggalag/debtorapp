package com.jyggalag.debtorappwellw.ui.foreign_debts

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jyggalag.debtorappwellw.R
import com.jyggalag.debtorappwellw.foundations.BaseRecyclerViewAdapter
import com.jyggalag.debtorappwellw.models.ForeignDebt
import com.jyggalag.debtorappwellw.ui.display.DisplayDebtActivity
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity.Companion.FRAGMENT_FOREIGN_DEBT
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity.Companion.FRAGMENT_TYPE_KEY
import com.jyggalag.debtorappwellw.ui.views.ForeignDebtView

class ForeignDebtAdapter(
    foreignDebts: MutableList<ForeignDebt> = mutableListOf(),
    val dataActionDelegate: ForeignDebtListViewContract
): BaseRecyclerViewAdapter<ForeignDebt>(foreignDebts) {

    private var onDebtSelectListener: OnDebtSelectListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ForeignDebtViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_foreign_debt_list, parent, false))


    inner class ForeignDebtViewHolder(view: View): BaseViewHolder<ForeignDebt>(view) {
        override fun onBind(data: ForeignDebt, listIndex: Int) {
            (view as ForeignDebtView).initView(data) {
                dataActionDelegate.onDeleteForeignDebt(listIndex.toLong())
            }
        }

        override var debtPosition: Int = 0
        init {
            view.setOnClickListener {
                onDebtSelectListener?.onDebtSelect(masterList[debtPosition])
                Log.d("UID From adapter", "UID: ${masterList[debtPosition].uid}")
                val intent = Intent(it.context, DisplayDebtActivity::class.java)
                intent.putExtra("owner", masterList[debtPosition].owner)
                intent.putExtra("deadline", masterList[debtPosition].deadline)
                intent.putExtra("debtType", masterList[debtPosition].debtType)
                intent.putExtra("partnerEmail", masterList[debtPosition].partnerEmail)
                intent.putExtra("partnerName", masterList[debtPosition].partnerName)
                intent.putExtra("partnerPhone", masterList[debtPosition].partnerPhone)
                intent.putExtra("startTime", masterList[debtPosition].startTime)
                intent.putExtra(FRAGMENT_TYPE_KEY, FRAGMENT_FOREIGN_DEBT)
                intent.putExtra("uuid", (masterList[debtPosition].uid).toString())
                it.context.startActivity(intent)
            }
        }
    }

    interface OnDebtSelectListener {
        fun onDebtSelect(foreignDebt: ForeignDebt)
    }
}