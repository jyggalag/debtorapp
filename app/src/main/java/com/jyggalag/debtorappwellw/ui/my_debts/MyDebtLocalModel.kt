package com.jyggalag.debtorappwellw.ui.my_debts

import android.util.Log
import com.jyggalag.debtorappwellw.application.DebtorAppApplication
import com.jyggalag.debtorappwellw.database.RoomDatabaseClient
import com.jyggalag.debtorappwellw.models.MyDebt
import kotlinx.coroutines.*
import javax.inject.Inject

const val TIMEOUT_DURATION_MILLIS = 3000L

class MyDebtLocalModel @Inject constructor(): IMyDebtModel {

    private var databaseClient: RoomDatabaseClient = RoomDatabaseClient.getInstance(
        DebtorAppApplication.instance.applicationContext
    )

    /**
     * method for using coroutines
     */
    private fun performOperationWithTimeout(function: () -> Unit, callback: SuccessCallback) {
        GlobalScope.launch {
            val job = async {
                try {
                    withTimeout(TIMEOUT_DURATION_MILLIS) {
                        function.invoke()
                    }
                } catch (e: Exception) {
                    callback.invoke(false)
                }
            }
            job.await()
            callback.invoke(true)
        }
    }

    override suspend fun addMyDebt(myDebt: MyDebt, callback: SuccessCallback) {
        Log.d("MyDebts_add", myDebt.toString())
        performOperationWithTimeout({databaseClient.myDebtDAO().addMyDebt(myDebt)}, callback)
    }

    override suspend fun updateMyDebt(myDebt: MyDebt, callback: SuccessCallback) {
        Log.d("MyDebts_update", myDebt.toString())
        performOperationWithTimeout({databaseClient.myDebtDAO().updateMyDebt(myDebt)}, callback)
    }

    override suspend fun deleteMyDebt(uid: Long, callback: SuccessCallback) {
        Log.d("MyDebts_delete", uid.toString())
        performOperationWithTimeout({databaseClient.myDebtDAO().deleteMyDebt(uid)}, callback)
    }

    fun updateDebtValue(uid: Long, value: String, callback: SuccessCallback) {
        Log.d("MyDebts_update value", "$uid | value: $value")
        performOperationWithTimeout({databaseClient.myDebtDAO().updateMyDebtValue(uid, value)}, callback)
    }

    override suspend fun retrieveMyDebt(owner:String, callback: (List<MyDebt>?) -> Unit) {
        val job = GlobalScope.async {
            withTimeoutOrNull(TIMEOUT_DURATION_MILLIS) {
                databaseClient.myDebtDAO().retrieveMyDebts(owner)
            }
        }
        callback.invoke(job.await())
    }
}