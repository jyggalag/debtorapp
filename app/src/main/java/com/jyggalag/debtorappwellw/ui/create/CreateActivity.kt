package com.jyggalag.debtorappwellw.create

import android.app.DatePickerDialog
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.SmsManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.jyggalag.debtorappwellw.R
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity
import kotlinx.android.synthetic.main.fragment_create_my_debt.*
import java.util.*

class CreateActivity : AppCompatActivity(), CreateMyDebtFragment.OnFragmentInteractionListener,
    CreateForeignDebtFragment.OnFragmentInteractionListener {

    private lateinit var owner: String

    private val permissionRequest = 101

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_debt_add)

        supportActionBar?.title = "Dodawanie długu"

        intent.getStringExtra(NavigationActivity.FRAGMENT_TYPE_KEY).run {
            if (this == NavigationActivity.FRAGMENT_MY_DEBT) {
                createFragment(CreateMyDebtFragment.newInstance())
            } else if (this == NavigationActivity.FRAGMENT_FOREIGN_DEBT) {
                createFragment(CreateForeignDebtFragment.newInstance())
            }
        }

        owner = intent.getStringExtra("owner")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.saveItem -> {
                supportFragmentManager.findFragmentById(R.id.fragmentHolder)?.run {
                    if (this is CreateMyDebtFragment) {
                        this.saveMyDebt(owner) { success ->
                            if (success) {
                                this@CreateActivity.supportFinishAfterTransition()
                            }
//                            else {
//                                Toast.makeText(this@CreateActivity, getString(R.string.toast_error_saving), Toast.LENGTH_SHORT).show()
//                            }
                        }
                    } else if (this is CreateForeignDebtFragment) {
                        this.saveForeignDebt(owner) { success ->
                            if (success) {
                                this@CreateActivity.supportFinishAfterTransition()
                            }
//                            else {
//                                Toast.makeText(this@CreateActivity, getString(R.string.toast_error_saving), Toast.LENGTH_SHORT).show()
//                            }
                        }
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentHolder, fragment)
            .commit()
    }

    override fun onFragmentInteraction() {
        TODO("Not yet implemented")
    }
}
