package com.jyggalag.debtorappwellw.ui.edit

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.ContactsContract
import android.text.InputType
import android.text.InputType.TYPE_CLASS_TEXT
import android.text.InputType.TYPE_NUMBER_FLAG_DECIMAL
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.jyggalag.debtorappwellw.R
import com.jyggalag.debtorappwellw.foundations.ApplicationScope
import com.jyggalag.debtorappwellw.foundations.DateUtils
import com.jyggalag.debtorappwellw.foundations.NullFieldChecker
import com.jyggalag.debtorappwellw.models.ForeignDebt
import com.jyggalag.debtorappwellw.ui.foreign_debts.IForeignDebtModel
import com.jyggalag.debtorappwellw.ui.foreign_debts.SuccessCallback
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity
import com.jyggalag.debtorappwellw.ui.views.EditForeignDebtView
import kotlinx.android.synthetic.main.fragment_create_foreign_debt.*
import kotlinx.android.synthetic.main.fragment_create_my_debt.*
import kotlinx.android.synthetic.main.fragment_edit_foreign_debt.*
import kotlinx.android.synthetic.main.fragment_edit_foreign_debt.btnDatePicker
import kotlinx.android.synthetic.main.fragment_edit_foreign_debt.editDeadline
import kotlinx.android.synthetic.main.fragment_edit_foreign_debt.editPartnerEmail
import kotlinx.android.synthetic.main.fragment_edit_foreign_debt.editPartnerName
import kotlinx.android.synthetic.main.fragment_edit_foreign_debt.editPartnerPhone
import kotlinx.android.synthetic.main.fragment_edit_foreign_debt.editValue
import kotlinx.android.synthetic.main.fragment_edit_foreign_debt.moneyImage
import kotlinx.android.synthetic.main.fragment_edit_foreign_debt.moneySwitch
import kotlinx.android.synthetic.main.fragment_edit_foreign_debt.titleCreateMyDebt
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import toothpick.Toothpick
import java.util.*
import javax.inject.Inject
import kotlin.math.roundToInt

class EditForeignDebtFragment : Fragment(), NullFieldChecker {

    @Inject
    lateinit var model: IForeignDebtModel

    lateinit var contentView: EditForeignDebtView

    private val cal: Calendar = Calendar.getInstance()
    private val PICK_CONTACT = 1
    private var listener: OnFragmentInteractionListener? = null
    private var currency: String = " zł"

    private lateinit var owner: String
    var deadline: String? = null
    var debtType: String? = null
    var partnerEmail: String? = null
    var partnerName: String? = null
    var partnerPhone: String? = null
    var startTime: String? = null
    var uidString: String? = null
    var uid: Long = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Toothpick.inject(this, ApplicationScope.scope)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_foreign_debt, container, false).apply {
            contentView = this as EditForeignDebtView
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        disableDeadlineEditText(editDeadline)

        titleCreateMyDebt.text = "Dług obcy - edycja"

        owner = arguments?.getString("owner").toString()
        partnerName = arguments?.getString("partnerName")
        partnerPhone = arguments?.getString("partnerPhone")
        partnerEmail = arguments?.getString("partnerEmail")
        debtType = arguments?.getString("debtType")
        deadline = arguments?.getString("deadline")
        uidString = arguments?.getString("uid")
        startTime = arguments?.getString("startTime")
        Log.d("EditForeignDebtFragment", "uuid: $uidString")

        uid = uidString.let {
            it!!.toLong()
        }

        Log.d("onCreateView", "debtTYpe: $debtType")
        debtType?.let {
            moneySwitch.isChecked = it.endsWith("zł")
        }
        Log.d("onCreateView", "debtTYpe after operation: ${debtType?.dropLast(3)}")

        editPartnerName.setText(this.partnerName)
        editPartnerPhone.setText(this.partnerPhone)

        buttonContactListEditFd.setOnClickListener {
            checkPermissionForReadContacts()
        }

        editPartnerEmail.setText(this.partnerEmail)
        if (this.debtType?.endsWith("zł")!!) {
            editValue.setText(this.debtType!!.dropLast(3))
        } else {
            editValue.setText(this.debtType)
        }
        editDeadline.setText(this.deadline)

        // create an OnDateSetListener
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, month)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                DateUtils.updateDateInView(editDeadline!!, cal)
            }

        // show datePicker with OnDateSetListener
        btnDatePicker.setOnClickListener {
            context?.let { it1 ->
                DatePickerDialog(
                    it1,
                    dateSetListener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)
                ).show()
            }
        }
//
        // switch behavior

        if (moneySwitch.isChecked) {
            moneyImage.setImageResource(R.drawable.ic_baseline_money_24)
            editValue.hint = getString(R.string.amount_value)
            currency = " zł"
            editValue.inputType = TYPE_NUMBER_FLAG_DECIMAL
        } else {
            moneyImage.setImageResource(R.drawable.ic_baseline_local_florist_24)
            editValue.hint = getString(R.string.content_name)
            currency = ""
            editValue.inputType = TYPE_CLASS_TEXT
        }
        moneySwitch.isClickable = false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_CONTACT && resultCode == Activity.RESULT_OK) {
            val contactUri = data?.data ?: return
            val projection = arrayOf(
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER)
            val cursor = requireContext().contentResolver.query(contactUri, projection,
                null, null, null)

            if (cursor != null && cursor.moveToFirst()) {
                val nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                val numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                val name = cursor.getString(nameIndex)
                val number = cursor.getString(numberIndex)

                editPartnerName.setText(name)
                editPartnerPhone.setText(number.replace("\\s".toRegex(), ""))
            }
            cursor?.close()
        }
    }


    private fun checkPermissionForReadContacts() {
        val permissionCheck = context?.let { ContextCompat.checkSelfPermission(it, android.Manifest.permission.READ_CONTACTS) }
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            searchContacts()
        } else {
            ActivityCompat.requestPermissions(context as Activity, arrayOf(android.Manifest.permission.READ_CONTACTS),
                NavigationActivity.PERMISSION_REQUEST
            )
        }
    }

    private fun searchContacts() {
        val intent = Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
        startActivityForResult(intent, NavigationActivity.PICK_CONTACT)
    }


    private fun disableDeadlineEditText(editText: EditText) {
        editText.isEnabled = false
    }

    fun updateForeignDebt(owner: String, callback: SuccessCallback) {
        GlobalScope.launch {
            updateForeignDebtData(owner)?.let { foreignDebt ->
                model.updateForeignDebt(foreignDebt) {
                    callback.invoke(true)
                }
            } ?: callback.invoke(false)
        }
    }

    private fun updateForeignDebtData(owner: String): ForeignDebt? = if (!hasNullField()) {
        Log.d("updateForeingDebtData", "updating method; uid: $uid")
        uid.let { uid ->
            startTime?.let { startTime ->

                val debtTypeInString: String = editValue.editableText.toString()
                val debtTypeInDouble: Double
                var formattedDebtType: String = ""
                if (currency.isNotEmpty()) {
                    debtTypeInDouble = debtTypeInString.toDouble()
                    formattedDebtType = ((debtTypeInDouble * 100).roundToInt() / 100.0).toString()
                } else {
                    formattedDebtType = editValue.editableText.toString()
                }

                ForeignDebt(
                    uid = uid,
                    owner = owner,
                    partnerName = editPartnerName.editableText.toString(),
                    partnerPhone = editPartnerPhone.editableText.toString(),
                    partnerEmail = editPartnerEmail.editableText.toString(),
                    debtType = formattedDebtType + currency,
                    deadline = editDeadline.editableText.toString(),
                    startTime = startTime
                )
            }
        }
    } else {
        Log.d("updateForeignDebtData", "data not updated, strange...")
        null
    }

    companion object {
        @JvmStatic
        fun newInstance() = EditForeignDebtFragment()
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction()
    }

    override fun hasNullField(): Boolean =
        editPartnerName.editableText.isNullOrEmpty() or
        editPartnerPhone.editableText.isNullOrEmpty() or
        editPartnerEmail.editableText.isNullOrEmpty() or
        editValue.editableText.isNullOrEmpty()


}