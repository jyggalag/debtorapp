package com.jyggalag.debtorappwellw.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.ktx.Firebase
import com.jyggalag.debtorappwellw.R
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_signup.progressBar

class SignUpActivity : AppCompatActivity() {

    private val nameTAG = this::class.java.toString()

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_signup)

        // initialize firebase
        auth = Firebase.auth

        // attempt to create account
        btnSignUp.setOnClickListener {

            // show progressBar
            progressBar.visibility = View.VISIBLE

            val email: String = sEmail.text.toString()
            val password: String = sPassword.text.toString()
            val name: String = sName.text.toString()
            createNewUser(email, password, name)
        }
    }

    private fun createNewUser(email: String, password: String, name: String) {
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                // sign in success, go to main activity
                // set username
                val user = auth.currentUser
                val profileUpdates = userProfileChangeRequest {
                    displayName = name
                }
                user!!.updateProfile(profileUpdates).addOnCompleteListener{ setNameTask ->
                    if (setNameTask.isSuccessful)
                        Log.d(nameTAG, "Username set to: $name")
                    else {
                        Log.d(nameTAG, "Name is not set")
                    }
                }

                Log.d(nameTAG, "user creation: success")
                startActivity(Intent(this, NavigationActivity::class.java))
                finish()
            } else {
                Log.w(nameTAG, "user creation: failure\n$email\n$password", task.exception)
                Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun changeActivityToLogin(view: View) {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}
