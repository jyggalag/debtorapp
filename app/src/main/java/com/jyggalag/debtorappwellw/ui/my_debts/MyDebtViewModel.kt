package com.jyggalag.debtorappwellw.ui.my_debts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.jyggalag.debtorappwellw.foundations.ApplicationScope
import com.jyggalag.debtorappwellw.models.MyDebt
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import toothpick.Toothpick
import javax.inject.Inject

class MyDebtViewModel: ViewModel(), MyDebtListViewContract {

    @Inject
    lateinit var model: MyDebtLocalModel

    private val auth: FirebaseAuth = Firebase.auth

    private val _myDebtListLiveData: MutableLiveData<MutableList<MyDebt>> = MutableLiveData()
    val myDebtListLiveData: LiveData<MutableList<MyDebt>> = _myDebtListLiveData

    init {
        Toothpick.inject(this, ApplicationScope.scope)
        loadData()
    }

    fun loadData() {
        GlobalScope.launch {
            model.retrieveMyDebt(auth.currentUser?.displayName.toString()) { nullableList ->
                nullableList?.let {
                    _myDebtListLiveData.postValue(it as MutableList<MyDebt>?)
                }
            }
        }
    }

    override fun onUpdateMyDebt(myDebt: MyDebt) {
        GlobalScope.launch {
            model.updateMyDebt(myDebt) {
                if (it) {
                    loadData()
                }
            }
        }
    }

    override fun onDeleteMyDebt(uid: Long) {
        GlobalScope.launch {
            model.deleteMyDebt(uid) {
                if (it) {
                    loadData()
                }
            }
        }
    }
}