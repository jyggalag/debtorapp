package com.jyggalag.debtorappwellw.ui.views

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.jyggalag.debtorappwellw.foundations.NullFieldChecker
import kotlinx.android.synthetic.main.fragment_create_foreign_debt.view.*

class CreateForeignDebtView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 1
) : ConstraintLayout(context, attrs, defStyleAttr), NullFieldChecker {
    override fun hasNullField(): Boolean =
        editPartnerName.editableText.isNullOrEmpty() or
        editPartnerPhone.editableText.isNullOrEmpty() or
        editPartnerEmail.editableText.isNullOrEmpty() or
        editValue.editableText.isNullOrEmpty() or
        editDeadline.editableText.isNullOrEmpty()
}