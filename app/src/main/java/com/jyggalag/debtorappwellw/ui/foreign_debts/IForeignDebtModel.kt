package com.jyggalag.debtorappwellw.ui.foreign_debts

import com.jyggalag.debtorappwellw.models.ForeignDebt

typealias SuccessCallback = (Boolean) -> Unit

interface IForeignDebtModel {
    suspend fun addForeignDebt(foreignDebt: ForeignDebt, callback: SuccessCallback)
    suspend fun updateForeignDebt(foreignDebt: ForeignDebt, callback: SuccessCallback)
    suspend fun deleteForeignDebt(uid: Long, callback: SuccessCallback)
    suspend fun retrieveForeignDebt(owner: String, callback: (List<ForeignDebt>?) -> Unit)
}