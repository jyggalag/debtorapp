package com.jyggalag.debtorappwellw.ui.my_debts

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jyggalag.debtorappwellw.R
import com.jyggalag.debtorappwellw.foundations.BaseRecyclerViewAdapter
import com.jyggalag.debtorappwellw.models.MyDebt
import com.jyggalag.debtorappwellw.ui.display.DisplayDebtActivity
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity.Companion.FRAGMENT_MY_DEBT
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity.Companion.FRAGMENT_TYPE_KEY
import com.jyggalag.debtorappwellw.ui.views.MyDebtView

class MyDebtsAdapter(
    myDebts: MutableList<MyDebt> = mutableListOf(),
    val dataActionDelegate: MyDebtListViewContract
//    val touchActionDelegate: MyDebtsFragment.TouchActionDelegate
) : BaseRecyclerViewAdapter<MyDebt>(myDebts) {

    private var onDebtSelectedListener: OnDebtSelectListener? = null

    /**
     * creates view for displaying individual item
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        MyDebtViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_my_debt_list, parent, false))

    /**
     * display data
     */
    inner class MyDebtViewHolder(view: View): BaseViewHolder<MyDebt>(view) {
        override fun onBind(data: MyDebt, listIndex: Int) {
            (view as MyDebtView).initView(data) {
                dataActionDelegate.onDeleteMyDebt(listIndex.toLong())
            }
        }
        override var debtPosition: Int = 0
        init {
            view.setOnClickListener {
                onDebtSelectedListener?.onDebtSelect(masterList[debtPosition])
                Log.d("UID From adapter", "UID: ${masterList[debtPosition].uid}")
                val intent = Intent(it.context, DisplayDebtActivity::class.java)
                intent.putExtra("owner", masterList[debtPosition].owner)
                intent.putExtra("deadline", masterList[debtPosition].deadline)
                intent.putExtra("debtType", masterList[debtPosition].debtType)
                intent.putExtra("partnerEmail", masterList[debtPosition].partnerEmail)
                intent.putExtra("partnerName", masterList[debtPosition].partnerName)
                intent.putExtra("partnerPhone", masterList[debtPosition].partnerPhone)
                intent.putExtra("startTime", masterList[debtPosition].startTime)
                intent.putExtra(FRAGMENT_TYPE_KEY, FRAGMENT_MY_DEBT)
                intent.putExtra("uuid", (masterList[debtPosition].uid).toString())
                it.context.startActivity(intent)
            }
        }
    }

    interface OnDebtSelectListener {
        fun onDebtSelect(myDebt: MyDebt)
    }
}