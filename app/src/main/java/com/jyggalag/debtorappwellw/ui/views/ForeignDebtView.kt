package com.jyggalag.debtorappwellw.ui.views

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.jyggalag.debtorappwellw.models.ForeignDebt
import kotlinx.android.synthetic.main.item_my_debt_list.view.*

class ForeignDebtView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 1
) : ConstraintLayout(context, attrs, defStyleAttr) {

    fun initView(foreignDebt: ForeignDebt, deleteButtonClickedCallback: () -> Unit) {
        textName.text = foreignDebt.partnerName
        textDebtType.text = foreignDebt.debtType
        textDeadline.text = foreignDebt.deadline
    }

}