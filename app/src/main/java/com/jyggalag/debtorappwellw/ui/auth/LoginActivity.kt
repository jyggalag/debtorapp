package com.jyggalag.debtorappwellw.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.jyggalag.debtorappwellw.R
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private val nameTAG = this::class.java.toString()
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = Firebase.auth

        // attempt to signIn
        btnLogIn.setOnClickListener {

            // show progressBar
            progressBar.visibility = View.VISIBLE

            val email: String = loginEmail.text.toString()
            val password: String = loginPass.text.toString()
            signIn(email, password)
        }
    }

    private fun signIn(email: String, password: String) {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                // sign in success, go to main activity
                Log.d(nameTAG, "login: success")

                startActivity(Intent(this, NavigationActivity::class.java))
                finish()
            } else {
                Log.w(nameTAG, "login: failure\n$email\n$password", task.exception)
                Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun changeActivityToSignUp(view: View) {
        startActivity(Intent(this, SignUpActivity::class.java))
        finish()
    }
}
