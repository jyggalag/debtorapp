package com.jyggalag.debtorappwellw.ui.foreign_debts

import com.jyggalag.debtorappwellw.models.ForeignDebt

interface ForeignDebtListViewContract {

    fun onUpdateForeignDebt(foreignDebt: ForeignDebt)
    fun onDeleteForeignDebt(uid: Long)

}