package com.jyggalag.debtorappwellw.ui.my_debts

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jyggalag.debtorappwellw.R
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity
import com.jyggalag.debtorappwellw.ui.views.MyDebtListView

class MyDebtsFragment : Fragment() {

    lateinit var viewModel: MyDebtViewModel
    lateinit var contentView: MyDebtListView
    private lateinit var touchActionDelegate: TouchActionDelegate

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is TouchActionDelegate) {
            touchActionDelegate = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_debts, container, false).apply {
            contentView = this as MyDebtListView
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
        setContentView()
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadData()
    }

    private fun setContentView() {
        contentView.initVIew(viewModel)
    }

    private fun bindViewModel() {
        viewModel = ViewModelProvider(this).get(MyDebtViewModel::class.java)
        viewModel.myDebtListLiveData.observe(viewLifecycleOwner, Observer { myDebtList ->
            contentView.updateList(myDebtList)
        })
    }

    companion object {
        fun newInstance() = MyDebtsFragment()
    }

    interface TouchActionDelegate {
        fun onAddButtonClicked(value: String)
    }

}