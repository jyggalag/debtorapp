package com.jyggalag.debtorappwellw.ui.my_debts

import com.jyggalag.debtorappwellw.models.MyDebt

typealias SuccessCallback = (Boolean) -> Unit

interface IMyDebtModel {
    suspend fun addMyDebt(myDebt: MyDebt, callback: SuccessCallback)
    suspend fun updateMyDebt(myDebt: MyDebt, callback: SuccessCallback)
    suspend fun deleteMyDebt(uid: Long, callback: SuccessCallback)
    suspend fun retrieveMyDebt(owner: String, callback: (List<MyDebt>?) -> Unit)
}