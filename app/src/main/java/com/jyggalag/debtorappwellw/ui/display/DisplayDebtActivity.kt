package com.jyggalag.debtorappwellw.ui.display

import android.app.AlertDialog
import android.app.PendingIntent
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.telephony.SmsManager
import android.telephony.SubscriptionManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.jyggalag.debtorappwellw.R
import com.jyggalag.debtorappwellw.foundations.MessageTo
import com.jyggalag.debtorappwellw.models.MyDebt
import com.jyggalag.debtorappwellw.ui.edit.EditActivity
import com.jyggalag.debtorappwellw.ui.foreign_debts.ForeignDebtLocalModel
import com.jyggalag.debtorappwellw.ui.foreign_debts.SuccessCallback
import com.jyggalag.debtorappwellw.ui.my_debts.MyDebtLocalModel
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity.Companion.FRAGMENT_FOREIGN_DEBT
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity.Companion.FRAGMENT_MY_DEBT
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity.Companion.FRAGMENT_TYPE_KEY
import kotlinx.android.synthetic.main.activity_display_debt.*
import kotlinx.android.synthetic.main.fragment_edit_my_debt.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception
import java.nio.charset.UnsupportedCharsetException
import javax.inject.Inject

class DisplayDebtActivity : AppCompatActivity() {

    @Inject
    lateinit var model: MyDebtLocalModel

    @Inject
    lateinit var fdModel: ForeignDebtLocalModel

    var owner: String? = null
    var deadline: String? = null
    var debtType: String? = null
    var partnerEmail: String? = null
    var partnerName: String? = null
    var partnerPhone: String? = null
    var startTime: String? = null
    var kindOfDebt: String? = null
    var uidString: String? = null
    var uid: Long = 0

    private var messageSms: String = ""
    private var moneyValue: Double = 0.0
    private val permissionRequest = 101

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_debt)

//        // get all data from adapter
        owner = intent.getStringExtra("owner")
        deadline = intent.getStringExtra("deadline")
        debtType = intent.getStringExtra("debtType")
        partnerEmail = intent.getStringExtra("partnerEmail")
        partnerName = intent.getStringExtra("partnerName")
        partnerPhone = intent.getStringExtra("partnerPhone")
        startTime = intent.getStringExtra("startTime")
        uidString = intent.getStringExtra("uuid")
        Log.d("DisplayDebt", "uuid: $uidString")

        uid = uidString.let {
            it!!.toLong()
        }

        // get kind of debt
        kindOfDebt = intent.getStringExtra(FRAGMENT_TYPE_KEY)
        setActionBarTitle(kindOfDebt)

        // check if money or thing
        if (debtType!!.endsWith("zł")) {
            textDebtTitle.text = debtType
            btnPartDebt.isVisible = true
        }

        textDebtTitle.text = debtType
        textWho.text = partnerName

        // check if myDebt or foreign debt
        textWhoLabel.text = if (kindOfDebt == FRAGMENT_MY_DEBT) {
            "Od kogo"
        } else {
            "Dla kogo"
        }

        textStartTime.text = startTime
        textDeadline.text = deadline

        model = MyDebtLocalModel()
        fdModel = ForeignDebtLocalModel()

        Log.d("DisplayAct", "Owner: $owner")

        btnPartDebt.setOnClickListener {
            val inflater = layoutInflater
            val dialogLayout = inflater.inflate(R.layout.alert_dialog_partial_payment, null)
            val editText = dialogLayout.findViewById<EditText>(R.id.editText)
            editText.setText("0")

            var oldValue = debtType!!.dropLast(3).toDouble()
            var subtractValue: Double = 0.0
            var newValue: Double = 0.0
            
            AlertDialog.Builder(this)
                .setTitle("Częściowa spłata długu (partner otzyma wiadomość SMS).")
                .setView(dialogLayout)
                .setPositiveButton(R.string.ok) {dialog, which ->
                    if (kindOfDebt == FRAGMENT_MY_DEBT) {
                        subtractValue = editText.text.toString().toDouble()
                        newValue = oldValue - subtractValue
                        newValue = Math.round(newValue * 100.0) / 100.0

                        if (newValue <= 0.0) {
                            createPaymentSMS()
                            deleteDebt("Wysłano informację o spłacie.")
                        } else {
                            updateMyDebtValue(uid, "$newValue zł") { success ->
                                if (success) {
                                    runOnUiThread {
                                        createPartialPayment("$newValue zł")
                                    }
                                    val intent = Intent(this@DisplayDebtActivity, NavigationActivity::class.java)
                                    intent.putExtra(FRAGMENT_TYPE_KEY, FRAGMENT_MY_DEBT)
                                    this@DisplayDebtActivity.startActivity(intent)
                                } else {
                                    Toast.makeText(applicationContext, "coś poszło nie tak", Toast.LENGTH_SHORT).show()
                                }
                            }
                        }
                    } else if (kindOfDebt == FRAGMENT_FOREIGN_DEBT) {
                        subtractValue = editText.text.toString().toDouble()
                        newValue = oldValue - subtractValue
                        newValue = Math.round(newValue * 100.0) / 100.0

                        if (newValue <= 0.0) {
                            createPaymentSMS()
                            deleteDebt("Wysłano informację o spłacie.")
                        } else {
                            updateForeignDebtValue(uid, "$newValue zł") { success ->
                                if (success) {
                                    runOnUiThread {
                                        createPartialPayment("$newValue zł")
                                    }
                                    val intent = Intent(this@DisplayDebtActivity, NavigationActivity::class.java)
                                    intent.putExtra(FRAGMENT_TYPE_KEY, FRAGMENT_FOREIGN_DEBT)
                                    this@DisplayDebtActivity.startActivity(intent)
                                } else {
                                    Toast.makeText(applicationContext, "coś poszło nie tak", Toast.LENGTH_SHORT).show()
                                }
                            }
                        }
                    }
                }
                .setNegativeButton(R.string.cancel) { dialog, which ->
                    Toast.makeText(applicationContext, "Anulowano spłatę", Toast.LENGTH_SHORT).show()
                }
                .show()
        }

        fabPayDebt.setOnClickListener {
//            finish()
//            startActivity(intent)
             AlertDialog.Builder(this)
                .setTitle("Spłata długu")
                .setMessage("Czy potwierdzić spłatę długu?\nPartner otrzyma wiadomość SMS.")
                .setPositiveButton(R.string.ok) {dialog, which ->
                    createPaymentSMS()
                    deleteDebt("Wysłano informację o spłacie.")
                }
                .setNegativeButton(R.string.cancel) { dialog, which ->
                    Toast.makeText(applicationContext, "Anulowano spłatę", Toast.LENGTH_SHORT).show()
                }
                .show()
        }
    }

    private fun updateMyDebtValue(uid: Long, value: String, callback: (Boolean) -> Unit) {
        GlobalScope.launch {
            Log.d("updateMyDebt", "updating method")

            model.updateDebtValue(uid, value) {
                callback.invoke(true)
            }
        }
    }

    private fun updateForeignDebtValue(uid: Long, value: String, callback: (Boolean) -> Unit) {
        GlobalScope.launch {
            Log.d("updateMyDebt", "updating method")

            fdModel.updateDebtValue(uid, value) {
                callback.invoke(true)
            }
        }
    }


    private fun createPaymentSMS() {
        messageSms = partnerName?.let {partnerName ->
            deadline?.let { deadline ->
                startTime?.let { startTime ->
                    debtType?.let { debtType ->
                        owner?.let { owner ->
                            kindOfDebt?.let { kindOfDebt ->
                                MessageTo.producePaymentSMS(
                                    partnerName,
                                    deadline,
                                    startTime,
                                    kindOfDebt,
                                    debtType,
                                    owner
                                )
                            }
                        }
                    }
                }
            }
        }.toString()
        partnerPhone?.let {
            sendSMS()
        }
    }

    private fun createPartialPayment(value: String) {
        messageSms = partnerName?.let {partnerName ->
            deadline?.let { deadline ->
                startTime?.let { startTime ->
                    value.let { value ->
                        owner?.let { owner ->
                            kindOfDebt?.let { kindOfDebt ->
                                MessageTo.producePartialPaymentSMS(
                                    partnerName,
                                    deadline,
                                    startTime,
                                    kindOfDebt,
                                    value,
                                    owner
                                )
                            }
                        }
                    }
                }
            }
        }.toString()
        partnerPhone?.let {
            sendSMS()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.display_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.delete -> {
                deleteDebt("Usunięto")
            }

            R.id.editDebt -> {
                intent = Intent(this, EditActivity::class.java)
                if (kindOfDebt == FRAGMENT_MY_DEBT) {
                    intent.putExtra(FRAGMENT_TYPE_KEY, FRAGMENT_MY_DEBT)
                } else if (kindOfDebt == FRAGMENT_FOREIGN_DEBT) {
                    intent.putExtra(FRAGMENT_TYPE_KEY, FRAGMENT_FOREIGN_DEBT)
                }
                intent.putExtra("uid", uidString)
                intent.putExtra("owner", owner)
                intent.putExtra("deadline", deadline)
                intent.putExtra("debtType", debtType)
                intent.putExtra("partnerEmail", partnerEmail)
                intent.putExtra("partnerName", partnerName)
                intent.putExtra("partnerPhone", partnerPhone)
                intent.putExtra("startTime", startTime)
                startActivity(intent)
            }

            R.id.sendEmail -> {
                val message = partnerName?.let {partnerName ->
                    deadline?.let {deadline ->
                        startTime?.let { startTime ->
                            debtType?.let { debtType ->
                                owner?.let { owner ->
                                    kindOfDebt?.let {kindOfDebt ->
                                        MessageTo.produceRemindMessage(
                                            partnerName, deadline, startTime, kindOfDebt, debtType, owner
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
                partnerEmail?.let {
                    if (message != null) {
                        sendMailToPartner(it, "Test", message)
                    }
                }
            }

            R.id.sendSMS -> {
                messageSms = partnerName?.let {partnerName ->
                    deadline?.let { deadline ->
                        startTime?.let { startTime ->
                            debtType?.let { debtType ->
                                owner?.let { owner ->
                                    kindOfDebt?.let { kindOfDebt ->
                                        MessageTo.produceRemindSMS(
                                            partnerName,
                                            deadline,
                                            startTime,
                                            kindOfDebt,
                                            debtType,
                                            owner
                                        )
                                    }
                                }
                            }
                        }
                    }
                }.toString()
                partnerPhone?.let {
                    sendSMS()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteDebt(message: String) {
        if (kindOfDebt == FRAGMENT_MY_DEBT) {
            Log.d(FRAGMENT_MY_DEBT, "Starting saving update")
            GlobalScope.launch {
                model.deleteMyDebt(uid) {
                    if (it) {
                        startActivity(Intent(this@DisplayDebtActivity, NavigationActivity::class.java))
                    }
                }
            }
            Toast.makeText(baseContext, message, Toast.LENGTH_SHORT).show()
        } else if (kindOfDebt == FRAGMENT_FOREIGN_DEBT) {
            Log.d(FRAGMENT_FOREIGN_DEBT, "Starting saving update")
            GlobalScope.launch {
                fdModel.deleteForeignDebt(uid) {
                    if (it) {
                        startActivity(Intent(this@DisplayDebtActivity, NavigationActivity::class.java))
                    }
                }
            }
            Toast.makeText(baseContext, message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun setActionBarTitle(kindOfDebt: String?) {
        if (kindOfDebt == FRAGMENT_MY_DEBT) {
            supportActionBar?.title = "Mój dług"
        } else if (kindOfDebt == FRAGMENT_FOREIGN_DEBT) {
            supportActionBar?.title = "Dług obcy"
        }
    }

    private fun sendMailToPartner(email: String, subject: String, messageText: String) {
        val intent = Intent(Intent.ACTION_SEND)
//        intent.data = Uri.parse("mailto:")
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        intent.putExtra(Intent.EXTRA_TEXT, messageText)
        intent.type = "message/rfc822"
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(Intent.createChooser(intent, "Wybierz aplikację"))
        }
    }

    private fun sendSMS() {
        val permissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.SEND_SMS)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            createAndSendSMS()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.SEND_SMS), permissionRequest)
        }
    }

    private fun createAndSendSMS() {
        val smsManager:SmsManager = SmsManager.getDefault()
        Log.d("createSMS", "Created message to $partnerPhone | $messageSms")
        val messageList: ArrayList<String> = smsManager.divideMessage(messageSms)
        smsManager.sendMultipartTextMessage(partnerPhone, null, messageList, null, null)
        Toast.makeText(this, "Wysłano wiadomość", Toast.LENGTH_SHORT).show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == permissionRequest) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createAndSendSMS()
                Log.d("onRequestBlahBlah", "Created message: $messageSms")
            } else {
                Toast.makeText(this, "Nie posiadasz uprawnień do wysłania wiadomości SMS", Toast.LENGTH_SHORT).show()
            }
        }
    }
}