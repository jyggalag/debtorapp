package com.jyggalag.debtorappwellw.ui.views

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.jyggalag.debtorappwellw.models.ForeignDebt
import com.jyggalag.debtorappwellw.ui.foreign_debts.ForeignDebtAdapter
import com.jyggalag.debtorappwellw.ui.foreign_debts.ForeignDebtListViewContract
import kotlinx.android.synthetic.main.fragment_foreign_debts.view.*

class ForeignDebtListView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 1
) : ConstraintLayout(context, attrs, defStyleAttr){

    private lateinit var adapter: ForeignDebtAdapter
    private lateinit var dataActionDelegate: ForeignDebtListViewContract

    fun initView(daDelegate: ForeignDebtListViewContract) {
        setupDelegates(daDelegate)
        setupView()
    }

    fun updateList(list: List<ForeignDebt>) {
        adapter.updateList(list)
    }

    private fun setupDelegates(daDelegate: ForeignDebtListViewContract) {
        dataActionDelegate = daDelegate
    }

    private fun setupView() {
        list_foreign_debts.layoutManager = LinearLayoutManager(context)
        adapter = ForeignDebtAdapter(dataActionDelegate = dataActionDelegate)
        list_foreign_debts.adapter = adapter
    }

}