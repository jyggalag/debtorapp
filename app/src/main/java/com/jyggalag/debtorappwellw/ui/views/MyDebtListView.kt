package com.jyggalag.debtorappwellw.ui.views

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.jyggalag.debtorappwellw.models.MyDebt
import com.jyggalag.debtorappwellw.ui.my_debts.MyDebtListViewContract
import com.jyggalag.debtorappwellw.ui.my_debts.MyDebtsAdapter
import kotlinx.android.synthetic.main.fragment_my_debts.view.*

class MyDebtListView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 1
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private lateinit var adapter: MyDebtsAdapter
    private lateinit var dataActionDelegate: MyDebtListViewContract

    fun initVIew(
        daDelegate: MyDebtListViewContract
    ) {
        setUpDelegates(daDelegate)
        setUpView()
    }

    fun updateList(list: List<MyDebt>) {
        adapter.updateList(list)
    }

    private fun setUpDelegates(
        daDelegate: MyDebtListViewContract
    ) {
        dataActionDelegate = daDelegate
    }

    private fun setUpView() {
        list_my_debts.layoutManager = LinearLayoutManager(context)
        adapter = MyDebtsAdapter(dataActionDelegate = dataActionDelegate)
        list_my_debts.adapter = adapter
    }

}