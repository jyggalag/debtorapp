package com.jyggalag.debtorappwellw.ui.foreign_debts

import android.util.Log
import com.jyggalag.debtorappwellw.application.DebtorAppApplication
import com.jyggalag.debtorappwellw.database.RoomDatabaseClient
import com.jyggalag.debtorappwellw.models.ForeignDebt
import kotlinx.coroutines.*
import java.lang.Exception
import javax.inject.Inject

const val TIMEOUT_DURATION_MILLIS = 3000L

class ForeignDebtLocalModel @Inject constructor(): IForeignDebtModel {

    private var databaseClient: RoomDatabaseClient = RoomDatabaseClient.getInstance(
        DebtorAppApplication.instance.applicationContext
    )

    /**
     *  Method for coroutines
     */
    private fun performOperationWithTimeout(function: () -> Unit, callback: SuccessCallback) {
        GlobalScope.launch {
            var job = async {
                try {
                    withTimeout(TIMEOUT_DURATION_MILLIS) {
                        function.invoke()
                    }
                } catch (e: Exception) {
                    callback.invoke(false)
                }
            }
            job.await()
            callback.invoke(true)
        }
    }

    override suspend fun addForeignDebt(foreignDebt: ForeignDebt, callback: SuccessCallback) {
        Log.d("ForeignDebt add", foreignDebt.toString())
        performOperationWithTimeout({databaseClient.foreignDebtDAO().addForeignDebt(foreignDebt)}, callback)
    }

    override suspend fun updateForeignDebt(foreignDebt: ForeignDebt, callback: SuccessCallback) {
        Log.d("ForeignDebt update", foreignDebt.toString())
        performOperationWithTimeout({databaseClient.foreignDebtDAO().updateForeignDebt(foreignDebt)}, callback)
    }

    override suspend fun deleteForeignDebt(uid: Long, callback: SuccessCallback) {
        Log.d("ForeignDebt delete", "Debt UID: $uid")
        performOperationWithTimeout({databaseClient.foreignDebtDAO().deleteForeignDebt(uid)}, callback)
    }

    fun updateDebtValue(uid: Long, value: String, callback: SuccessCallback) {
        Log.d("MyDebts_update value", "$uid | value: $value")
        performOperationWithTimeout({databaseClient.foreignDebtDAO().updateForeignDebtValue(uid, value)}, callback)
    }

    override suspend fun retrieveForeignDebt(owner: String, callback: (List<ForeignDebt>?) -> Unit) {
        val job = GlobalScope.async {
            withTimeoutOrNull(TIMEOUT_DURATION_MILLIS) {
                databaseClient.foreignDebtDAO().retrieveForeignDebts(owner)
            }
        }
        callback.invoke(job.await())
    }

}