package com.jyggalag.debtorappwellw.ui.views

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.jyggalag.debtorappwellw.models.MyDebt
import kotlinx.android.synthetic.main.item_my_debt_list.view.*

class MyDebtView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 1
) : ConstraintLayout(context, attrs, defStyleAttr) {

    fun initView(myDebt: MyDebt, deleteButtonClickedCallback: () -> Unit) {
        textName.text = myDebt.partnerName
        textDebtType.text = myDebt.debtType
        textDeadline.text = myDebt.deadline
    }
}