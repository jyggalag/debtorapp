package com.jyggalag.debtorappwellw.create

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Contacts
import android.provider.ContactsContract
import android.text.InputType.*
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.jyggalag.debtorappwellw.R
import com.jyggalag.debtorappwellw.foundations.ApplicationScope
import com.jyggalag.debtorappwellw.foundations.DateUtils
import com.jyggalag.debtorappwellw.foundations.DateUtils.updateDateInView
import com.jyggalag.debtorappwellw.foundations.NullFieldChecker
import com.jyggalag.debtorappwellw.models.MyDebt
import com.jyggalag.debtorappwellw.ui.my_debts.IMyDebtModel
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity
import com.jyggalag.debtorappwellw.ui.views.CreateMyDebtView
import kotlinx.android.synthetic.main.fragment_create_foreign_debt.*
import kotlinx.android.synthetic.main.fragment_create_my_debt.*
import kotlinx.android.synthetic.main.fragment_create_my_debt.btnDatePicker
import kotlinx.android.synthetic.main.fragment_create_my_debt.editDeadline
import kotlinx.android.synthetic.main.fragment_create_my_debt.editPartnerEmail
import kotlinx.android.synthetic.main.fragment_create_my_debt.editPartnerName
import kotlinx.android.synthetic.main.fragment_create_my_debt.editPartnerPhone
import kotlinx.android.synthetic.main.fragment_create_my_debt.editValue
import kotlinx.android.synthetic.main.fragment_create_my_debt.moneyImage
import kotlinx.android.synthetic.main.fragment_create_my_debt.moneySwitch
import kotlinx.android.synthetic.main.fragment_edit_my_debt.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import toothpick.Toothpick
import java.util.*
import javax.inject.Inject
import kotlin.math.roundToInt

class CreateMyDebtFragment : Fragment(), NullFieldChecker {

    private val PICK_CONTACT = 1

    @Inject
    lateinit var model: IMyDebtModel

    lateinit var contentView: CreateMyDebtView

    private val cal: Calendar = Calendar.getInstance()

    private var editTextDeadLine: EditText? = null
    private var listener: OnFragmentInteractionListener? = null
    private var currency: String = " zł"

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Toothpick.inject(this, ApplicationScope.scope)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_my_debt, container, false).apply {
            contentView = this as CreateMyDebtView
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        disableDeadlineEditText(editDeadline)

        var number: String
        buttonContactListCreateMd.setOnClickListener {
            checkPermissionForReadContacts()
        }

        // get reference from layout
        editTextDeadLine = this.editDeadline
        val btnDate: Button = btnDatePicker
        editTextDeadLine!!.hint = DateUtils.setToday()

        // create an OnDateSetListener
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, month)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView(editTextDeadLine!!, cal)
            }

        // show datePicker with OnDateSetListener
        btnDate.setOnClickListener {
            context?.let { it1 ->
                DatePickerDialog(
                    it1,
                    dateSetListener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)
                ).show()
            }
        }

        // switch behavior
        moneySwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                moneyImage.setImageResource(R.drawable.ic_baseline_money_24)
                editValue.hint = getString(R.string.amount_value)
                currency = " zł"
                editValue.inputType = TYPE_NUMBER_FLAG_DECIMAL
            } else {
                moneyImage.setImageResource(R.drawable.ic_baseline_local_florist_24)
                editValue.hint = getString(R.string.content_name)
                currency = ""
                editValue.inputType = TYPE_CLASS_TEXT
            }
        }
    }

    private fun checkPermissionForReadContacts() {
        val permissionCheck = context?.let { ContextCompat.checkSelfPermission(it, android.Manifest.permission.READ_CONTACTS) }
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            searchContacts()
        } else {
            ActivityCompat.requestPermissions(context as Activity, arrayOf(android.Manifest.permission.READ_CONTACTS),
                NavigationActivity.PERMISSION_REQUEST
            )
        }
    }

    private fun searchContacts() {
        val intent = Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
        startActivityForResult(intent, NavigationActivity.PICK_CONTACT)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_CONTACT && resultCode == Activity.RESULT_OK) {
            val contactUri = data?.data ?: return
            val projection = arrayOf(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER)
            val cursor = requireContext().contentResolver.query(contactUri, projection,
                null, null, null)

            if (cursor != null && cursor.moveToFirst()) {
                val nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                val numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                val name = cursor.getString(nameIndex)
                val number = cursor.getString(numberIndex)

                editPartnerName.setText(name)
                editPartnerPhone.setText(number.replace("\\s".toRegex(), ""))
            }
            cursor?.close()
        }
    }

    private fun disableDeadlineEditText(editText: EditText) {
        editText.isEnabled = false
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun saveMyDebt(owner: String, callback: (Boolean) -> Unit) {
        GlobalScope.launch {
            createMyDebt(owner)?.let {
                model.addMyDebt(it) {
                    callback.invoke(true)
                }
            } ?: callback.invoke(false)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createMyDebt(owner: String): MyDebt? = if (!hasNullField()) {

        val debtTypeInString: String = editValue.editableText.toString()
        val debtTypeInDouble: Double
        var formattedDebtType: String = ""
        if (currency.isNotEmpty()) {
            debtTypeInDouble = debtTypeInString.toDouble()
            formattedDebtType = ((debtTypeInDouble * 100).roundToInt() / 100.0).toString()
        } else {
            formattedDebtType = editValue.editableText.toString()
        }

        MyDebt(
            uid =  UUID.randomUUID().leastSignificantBits,
            owner = owner,
            partnerName = editPartnerName.editableText.toString(),
            partnerPhone = editPartnerPhone.editableText.toString(),
            partnerEmail = editPartnerEmail.editableText.toString(),
            debtType = formattedDebtType + currency,
            deadline = editDeadline.editableText.toString(),
            startTime = DateUtils.setToday()
        )
    } else {
        null
    }

    companion object {
        fun newInstance() = CreateMyDebtFragment()
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction()
    }

    override fun hasNullField(): Boolean =
        editPartnerName.editableText.isNullOrEmpty() or
        editPartnerPhone.editableText.isNullOrEmpty() or
        editPartnerEmail.editableText.isNullOrEmpty() or
        editValue.editableText.isNullOrEmpty() or
        editDeadline.editableText.isNullOrEmpty()

}