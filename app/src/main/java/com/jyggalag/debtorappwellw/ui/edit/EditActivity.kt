package com.jyggalag.debtorappwellw.ui.edit

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.jyggalag.debtorappwellw.R
import com.jyggalag.debtorappwellw.create.CreateMyDebtFragment
import com.jyggalag.debtorappwellw.ui.display.DisplayDebtActivity
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity.Companion.FRAGMENT_FOREIGN_DEBT
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity.Companion.FRAGMENT_MY_DEBT
import com.jyggalag.debtorappwellw.ui.navigation.NavigationActivity.Companion.FRAGMENT_TYPE_KEY

class EditActivity : AppCompatActivity(), EditMyDebtFragment.OnFragmentInteractionListener, EditForeignDebtFragment.OnFragmentInteractionListener {
    private lateinit var owner: String
    var deadline: String? = null
    var debtType: String? = null
    var partnerEmail: String? = null
    var partnerName: String? = null
    var partnerPhone: String? = null
    var startTime: String? = null
    var kindOfDebt: String? = null
    var uid: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_debt_add)

        supportActionBar?.title = ""

        owner = intent.getStringExtra("owner")
        deadline = intent.getStringExtra("deadline")
        debtType = intent.getStringExtra("debtType")
        partnerEmail = intent.getStringExtra("partnerEmail")
        partnerName = intent.getStringExtra("partnerName")
        partnerPhone = intent.getStringExtra("partnerPhone")
        startTime = intent.getStringExtra("startTime")
        uid = intent.getStringExtra("uid")

        val bundle: Bundle = bundleOf()
        bundle.putString("owner", owner)
        bundle.putString("deadline", deadline)
        bundle.putString("debtType", debtType)
        bundle.putString("partnerEmail", partnerEmail)
        bundle.putString("partnerName", partnerName)
        bundle.putString("partnerPhone", partnerPhone)
        bundle.putString("startTime", startTime)
        bundle.putString("uid", uid)

        intent.getStringExtra(FRAGMENT_TYPE_KEY).run {
            if (this == FRAGMENT_MY_DEBT) {
                createFragment(EditMyDebtFragment.newInstance(), bundle)
            }
            else if (this == FRAGMENT_FOREIGN_DEBT) {
                createFragment(EditForeignDebtFragment.newInstance(), bundle)
            }
        }

        Log.d("EditAct", "Owner: $owner")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.saveItem -> {
                supportFragmentManager.findFragmentById(R.id.fragmentHolder)?.run {
                    if (this is EditMyDebtFragment) {
                        this.updateMyDebt(owner) { success ->
                            if (success) {
//                                this@EditActivity.supportFinishAfterTransition()
                                activity?.let {
                                    val intent = Intent(it, NavigationActivity::class.java)
                                    intent.putExtra(FRAGMENT_TYPE_KEY, FRAGMENT_MY_DEBT)
                                    it.startActivity(intent)
                                }
                            } else {
                                Toast.makeText(
                                    this@EditActivity,
                                    getString(R.string.toast_error_saving),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    } else if (this is EditForeignDebtFragment) {
                        this.updateForeignDebt(owner) { success ->
                            if (success) {
//                                this@EditActivity.supportFinishAfterTransition()
                                activity?.let {
                                    val intent = Intent(it, NavigationActivity::class.java)
                                    intent.putExtra(FRAGMENT_TYPE_KEY, FRAGMENT_FOREIGN_DEBT)
                                    it.startActivity(intent)
                                }
                            } else {
                                Toast.makeText(
                                    this@EditActivity,
                                    getString(R.string.foreignDebt_edit_error),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createFragment(fragment: Fragment, bundle: Bundle) {
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentHolder, fragment)
            .commit()
    }

    override fun onFragmentInteraction() {
        TODO("Not yet implemented")
    }
}