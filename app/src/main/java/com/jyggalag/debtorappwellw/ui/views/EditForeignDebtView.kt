package com.jyggalag.debtorappwellw.ui.views

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.jyggalag.debtorappwellw.models.ForeignDebt
import kotlinx.android.synthetic.main.fragment_edit_foreign_debt.view.*

class EditForeignDebtView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 1
) : ConstraintLayout(context, attrs, defStyleAttr) {

    fun initView(foreignDebt: ForeignDebt) {
        editPartnerName.setText(foreignDebt.partnerName)
    }
}