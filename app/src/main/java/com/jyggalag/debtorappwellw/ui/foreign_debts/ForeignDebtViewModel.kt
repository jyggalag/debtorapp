package com.jyggalag.debtorappwellw.ui.foreign_debts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.jyggalag.debtorappwellw.foundations.ApplicationScope
import com.jyggalag.debtorappwellw.models.ForeignDebt
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import toothpick.Toothpick
import javax.inject.Inject

class ForeignDebtViewModel: ViewModel(), ForeignDebtListViewContract {

    @Inject
    lateinit var model: ForeignDebtLocalModel

    private var auth: FirebaseAuth = Firebase.auth

    private val _foreignDebtListLiveData: MutableLiveData<MutableList<ForeignDebt>> = MutableLiveData()
    val foreignDebtListLiveData: LiveData<MutableList<ForeignDebt>> = _foreignDebtListLiveData

    init {
        Toothpick.inject(this, ApplicationScope.scope)
        loadData()
    }

    fun loadData() {
       GlobalScope.launch {
           model.retrieveForeignDebt(auth.currentUser?.displayName.toString()) { nullableList ->
               nullableList?.let {
                   _foreignDebtListLiveData.postValue(it as MutableList<ForeignDebt>?)
               }
           }
       }
    }

    override fun onUpdateForeignDebt(foreignDebt: ForeignDebt) {
        GlobalScope.launch {
            model.updateForeignDebt(foreignDebt) {
                if (it) {
                    loadData()
                }
            }
        }
    }

    override fun onDeleteForeignDebt(uid: Long) {
        GlobalScope.launch {
            model.deleteForeignDebt(uid) {
                if (it) {
                    loadData()
                }
            }
        }
    }
}