package com.jyggalag.debtorappwellw.ui.views

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.jyggalag.debtorappwellw.models.MyDebt
import kotlinx.android.synthetic.main.fragment_edit_my_debt.view.*

class EditMyDebtView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 1
) : ConstraintLayout(context, attrs, defStyleAttr) {

    fun initView(myDebt: MyDebt) {
        editPartnerName.setText(myDebt.partnerName)
    }
}