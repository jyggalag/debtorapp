package com.jyggalag.debtorappwellw.ui.my_debts

import com.jyggalag.debtorappwellw.models.MyDebt

interface MyDebtListViewContract {

    fun onUpdateMyDebt(myDebt: MyDebt)
    fun onDeleteMyDebt(uid: Long)
}