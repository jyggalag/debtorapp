package com.jyggalag.debtorappwellw.ui.navigation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.jyggalag.debtorappwellw.R
import com.jyggalag.debtorappwellw.ui.auth.LoginActivity
import com.jyggalag.debtorappwellw.ui.foreign_debts.ForeignDebtsFragment
import com.jyggalag.debtorappwellw.create.CreateActivity
import com.jyggalag.debtorappwellw.ui.my_debts.MyDebtsFragment
import kotlinx.android.synthetic.main.activity_navigation.*

class NavigationActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var mAdView : AdView

    private var currentFragment: String? = null

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {

                R.id.myDebts -> {
                    replaceFragment(MyDebtsFragment.newInstance(), FRAGMENT_MY_DEBT)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.foreignDebts -> {
                    replaceFragment(ForeignDebtsFragment.newInstance(), FRAGMENT_FOREIGN_DEBT)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

        // initialize ADMob
        MobileAds.initialize(this) {}

        mAdView = findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)

        currentFragment = intent.getStringExtra(FRAGMENT_TYPE_KEY)?: FRAGMENT_FOREIGN_DEBT

        Log.d("Navigation act", "Fragment: $currentFragment")
        if (currentFragment == FRAGMENT_MY_DEBT) {
            replaceFragment(MyDebtsFragment.newInstance(), FRAGMENT_MY_DEBT)
        }
        else if (currentFragment == FRAGMENT_FOREIGN_DEBT) {
            replaceFragment(ForeignDebtsFragment.newInstance(), FRAGMENT_FOREIGN_DEBT)
        }

        navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        auth = Firebase.auth

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener {
            intent = Intent(this, CreateActivity::class.java)
            intent.putExtra(FRAGMENT_TYPE_KEY, checkCurrentFragment())
            intent.putExtra("owner", auth.currentUser?.displayName.toString())
            startActivity(intent)
        }
    }

    private fun checkCurrentFragment(): String {
        val fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_MY_DEBT)
        return if (fragment != null && fragment.isVisible) {
            FRAGMENT_MY_DEBT
        } else {
            FRAGMENT_FOREIGN_DEBT
        }
    }

    private fun replaceFragment(fragment: Fragment, tag: String) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentHolder, fragment, tag)
            .commit()
    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        val intent = Intent(this, LoginActivity::class.java)
        if (currentUser == null) {
            startActivity(intent)
            finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.settings_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.logoutItem -> {
                logOut()
                Toast.makeText(baseContext, "Pomyślnie wylogowano", Toast.LENGTH_SHORT).show()
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun logOut() {
        Firebase.auth.signOut()
    }

    override fun onBackPressed() {
        if (backPressedCounter < 1) {
            showMessage("Wciśnij ponownie, aby zakończyć.")
            backPressedCounter++
        } else {
            super.onBackPressed()
            finishAffinity()
        }
    }

    private fun showMessage(s: String) {
        Snackbar.make(navigationView, s, Snackbar.LENGTH_LONG).show()
    }

    companion object {
        const val FRAGMENT_TYPE_KEY = "f_t_k"
        const val FRAGMENT_MY_DEBT = "f_m_d"
        const val FRAGMENT_FOREIGN_DEBT = "f_f_d"
        const val PERMISSION_REQUEST = 1
        const val PICK_CONTACT = 1
        var backPressedCounter: Int = 0

    }
}