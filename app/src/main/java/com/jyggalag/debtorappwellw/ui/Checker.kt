package com.jyggalag.debtorappwellw.ui.my_debts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.jyggalag.debtorappwellw.R
import com.jyggalag.debtorappwellw.ui.auth.LoginActivity
import kotlinx.android.synthetic.main.activity_checker.*

class Checker : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checker)

        // initialize Firebase
        auth = Firebase.auth
    }

    override fun onStart() {
        super.onStart()
        // check if user is signed in and up
        val currentUser = auth.currentUser
        val intent = Intent(this, LoginActivity::class.java)
        if (currentUser == null) {
            startActivity(intent)
            finish()
        } else {
            userCheck.text = currentUser.displayName.toString()
            logOutBtn.setOnClickListener{
                logOut()
                startActivity(intent)
                finish()
            }

            btnReload.setOnClickListener {
                reloadActivity()
            }

        }
    }

    private fun logOut() {
        Firebase.auth.signOut()
    }

    private fun reloadActivity() {
        val refreshActivity = intent
        finish()
        startActivity(refreshActivity)
    }

}
