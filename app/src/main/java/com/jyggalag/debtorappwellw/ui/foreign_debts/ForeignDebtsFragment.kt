package com.jyggalag.debtorappwellw.ui.foreign_debts

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.jyggalag.debtorappwellw.R
import com.jyggalag.debtorappwellw.ui.my_debts.MyDebtsFragment
import com.jyggalag.debtorappwellw.ui.views.ForeignDebtListView

class ForeignDebtsFragment : Fragment() {

    lateinit var viewModel: ForeignDebtViewModel
    lateinit var contentView: ForeignDebtListView
    private lateinit var touchActionDelegate: TouchActionDelegate

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is TouchActionDelegate) {
            touchActionDelegate = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_foreign_debts, container, false).apply {
            contentView = this as ForeignDebtListView
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
        setContentView()
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadData()
    }

    private fun bindViewModel() {
        viewModel = ViewModelProvider(this).get(ForeignDebtViewModel::class.java)
        viewModel.foreignDebtListLiveData.observe(viewLifecycleOwner, Observer { foreignDebtList ->
            contentView.updateList(foreignDebtList)
        })
    }

    private fun setContentView() {
        contentView.initView(viewModel)
    }

    companion object {
        fun newInstance() = ForeignDebtsFragment()
    }

    interface TouchActionDelegate {
        fun onAddButtonClicked(value: String)
    }
}