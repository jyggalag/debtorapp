package com.jyggalag.debtorappwellw

import android.os.SystemClock
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.jyggalag.debtorappwellw.ui.auth.LoginActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.DrawerActions
import androidx.test.espresso.contrib.NavigationViewActions
import androidx.test.espresso.matcher.ViewMatchers.*

@RunWith(AndroidJUnit4::class)
class LoginTest {

    @get:Rule
    val loginActivityRule = ActivityScenarioRule(LoginActivity::class.java)

    /**
     * provide correct credentials
     * press "login in" button
     * make sure there is transition to DebtListActivity
     * check if current user displayName is correct
     *      open navigationDrawer
     *      check current user
     * logout
     *
     */
    @Test
    fun loginIntoExistingAccount() {
        val emailAddress = "asd@asd.pl"
        val password = "123456"

        // type in fields
        onView(withId(R.id.loginEmail)).perform(typeText(emailAddress))
        onView(withId(R.id.loginPass)).perform(typeText(password))

        // hide keyboard
        closeSoftKeyboard()

        // hit login in button
        onView(withId(R.id.btnLogIn)).perform(click())

        // wait 3 seconds
        SystemClock.sleep(3000)

        // open NavigationDrawer
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open())

        // press "Show current username"
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_show_user))

        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText(R.string.logged_as)))

        // wait for snackbar hide
        SystemClock.sleep(3000)

        // check after pressingBack snackbar
        pressBack()
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText(R.string.click_to_quit)))

        // wait for snackbar hide
        SystemClock.sleep(3000)

        // logout - open NavigationDrawer
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open())

        // press logout
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_logout))

        // wait some time
        SystemClock.sleep(2000)

        // check if login in button is present
        onView(withId(R.id.btnLogIn)).check(matches(isDisplayed()))

    }

}